@extends('layouts.admin')

@section('title', 'Ticket Detail')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header mb-1">
                        <div class="card-title">
                            <h3>Ticket: {{ $data->title }}</h3>
                            <div>{!! $data->status_name  !!} {{ $data->noTicket }}</div>
                        </div>
                        <div>
                            <a href="{{ route('ticket.show', $data->id) }}">Lihat Detail</a>
                        </div>

                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Laporan</h4>
                    </div>
                    <div class="card-body">
                        @if(count($data->reports) > 0)
                            @foreach($data->reports as $report)
                                <div class="d-flex align-items-start mb-3">
                            <span class="mr-1">
                                @if ($report->teknisi->avatar)
                                    <img class="round" src="{{ asset('storage/' . $report->teknisi->avatar ) }}" alt="avatar" height="40" width="40">
                                @else
                                    <img class="round" src="{{ asset('images/logo/avatar-no.png') }}" alt="avatar" height="40" width="40" />
                                @endif

                            </span>
                                    <div class="mt-0 w-100">
                                        <div>{{ $report->teknisi->name }} <span class="badge badge-light">{{ $report->teknisi->role->name }}</span> <span class="float-right">{{ \Carbon\Carbon::parse($report->created_at)->diffForHumans() }}</span></div>
                                        <div class="p-2 mt-1 border border-accent-1 bg-white rounded">{{ $report->description }}</div>

                                        @if (($report ?? '') && (null != $report->attachment))
                                            <img src="{{ asset('storage/' . $report->attachment) }}" alt="" class="img-fluid rounded mt-2">
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="alert alert-light">Tidak ada data</div>
                        @endif
                    </div>
                </div>

                    @if ($data->status == 0 or $data->status == 1)

                        @if(\Illuminate\Support\Facades\Auth::user()->id == $data->teknisi_id)

                    <div class="card">
                        <form action="{{route('report.store')}}" enctype="multipart/form-data" method="POST" class="form">
                        @csrf
                        <div class="card-header">
                            <h4 class="card-title">Buat Laporan</h4>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <input type="hidden" name="ticket_id" value="{{ $data->id }}">
                            <textarea class="form-control w-100" name="comment" required placeholder="Write here..."></textarea>
                                <div class="custom-file mt-2">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01" name="attachment" value="{{ old('attachment') }}">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                </div>
                        </div>
                        </form>
                    </div>
                        @endif

                    @endif
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
@endsection
