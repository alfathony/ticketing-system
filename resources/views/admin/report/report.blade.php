@extends('layouts.admin')

@section('title', 'Report')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header mb-1">
                        <h4 class="card-title">List Report</h4>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <div class="row">
                                <div class="col-md-3 mb-2">
                                    <select id="filterTopic" class="form-control">
                                        <option value="0">Pilih Topik</option>
                                        @foreach($topics as $val)
                                            <option value="{{ $val->id }}" {{ isset($_GET['topic']) && ($_GET['topic'] == $val->id) ? 'selected' : '' }}>{{ $val->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped zero-configuration">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Status</th>
                                        <th>Tiket</th>
                                        <th>Topic</th>
                                        <th>Teknisi</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($datas as $val)

                                        <tr>
                                            <td>{{ $val->id }}</td>
                                            <td>{!!$val->StatusName !!}</td>
                                            <td>{{ $val->title }}</td>
                                            <td>{{ $val->topic->name }}</td>
                                            <td>{{ $val->teknisi->name }}</td>
                                            <td>
                                                <a href="{{ route('report.show', $val->id) }}" class="btn btn-primary btn-sm">Detail</a>
                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>

                                </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>

    <script>
        $('#filterTopic').on('change', function (){
            var valueTopic = $('#filterTopic').val();
            if (valueTopic == 0){
                window.location.href='report'
            }else{
                window.location.href='?topic=' + valueTopic
            }
        });
    </script>
@endsection
