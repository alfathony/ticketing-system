@extends('layouts.admin')

@section('title', 'Ticket Detail')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @can('isAdmin')

                @endcan
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <h2>Detail Tiket</h2>
                        </div>
                        <div class="text-right">
                            <a href="{{ route('cetak.ticket', $data->id) }}" class="btn btn-outline-microsoft">Cetak</a>
                            @cannot('isTeknisi')
                            <a href="{{ route('ticket.edit', $data->id) }}" class="btn btn-primary">Ubah</a>
                            <form action="{{ route('ticket.destroy', $data->id) }}" method="POST" class="d-inline">
                                @method('DELETE')
                                @csrf
                                <input type="submit" value="Hapus" class="btn btn-danger" onclick="return confirm('Anda yakin akan menghapus data?')">
                            </form>
                            @endcannot
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <div class="row mb-1">
                            <div class="col-md-3"><strong>Nomor Ticket</strong></div>
                            <div class="col-md-6">{{ $data->noticket }}</div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-md-3"><strong>Judul</strong></div>
                            <div class="col-md-6">{{ $data->title }}</div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-md-3"><strong>Topik</strong></div>
                            <div class="col-md-6">{{ $data->topic->name }}</div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-md-3"><strong>Tanggal</strong></div>
                            <div class="col-md-6">{{ \Carbon\Carbon::parse($data->created_at)->diffForHumans() }}</div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-3"><strong>Status</strong></div>
                            <div class="col-md-6">{!! $data->status_name  !!}</div>
                        </div>
                        <hr>
                        <div class="d-flex align-items-start mb-1">
                            <span class="mr-1">
                                @if ($data->user->avatar)
                                <img class="round" src="{{ asset('storage/' . $data->user->avatar ) }}" alt="avatar" height="40" width="40">
                                @else
                                    <img class="round" src="{{ asset('images/logo/avatar-no.png') }}" alt="avatar" height="40" width="40" />
                                @endif
                            </span>
                            <div class="mt-0 w-100">
                                <div>{{ $data->user->name }} <span class="badge badge-light">Author</span></div>
                                <div class="pt-1 px-2 mt-1 border border-accent-1 bg-white rounded">{!! $data->description !!}</div>

                                @if (($data ?? '') && (null != $data->attachment))
                                    <img src="{{ asset('storage/' . $data->attachment) }}" alt="" class="img-fluid rounded mt-2">
                                @endif

                                <hr>

                                <div class="row">
                                    <div class="col-md-4">
                                        <h6>Ditugaskan kepada </h6>
                                        @if ($data->teknisi_id )
                                            <p>{{ $data->teknisi->name }} | <a href="{{ route('report.show', $data->id) }}">Lihat Laporan</a></p>
                                        @else
                                            <p>Belum ditugaskan</p>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <h6>Tenggat Waktu </h6>
                                        @if ($data->deadline )
                                            <p>{{ \Carbon\Carbon::parse($data->deadline)->format('D, d M Y') }}</p>
                                        @else
                                            <p>Belum ditugaskan</p>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Comments</h4>
                    </div>
                    <div class="card-body">
                        @if(count($data->comments) > 0)
                            @foreach($data->comments as $comment)
                                <div class="d-flex align-items-start mb-3">
                            <span class="mr-1">
                                @if ($comment->user->avatar)
                                    <img class="round" src="{{ asset('storage/' . $comment->user->avatar ) }}" alt="avatar" height="40" width="40">
                                @else
                                    <img class="round" src="{{ asset('images/logo/avatar-no.png') }}" alt="avatar" height="40" width="40" />
                                @endif

                            </span>
                                    <div class="mt-0 w-100">
                                        <div>{{ $comment->user->name }} <span class="badge badge-light">{{ $comment->user->role->name }}</span> <span class="float-right">{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</span></div>
                                        <div class="p-2 mt-1 border border-accent-1 bg-white rounded">{{ $comment->comment }}</div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="alert alert-light">Tidak ada data</div>
                        @endif
                    </div>
                </div>

                    @if ($data->status == 0 or $data->status == 1)

                    <div class="card">
                        <form action="{{route('ticket-comments.store')}}" method="POST" class="form">
                        @csrf
                        <div class="card-header">
                            <h4 class="card-title">Comments</h4>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <input type="hidden" name="ticket_id" value="{{ $data->id }}">
                            <textarea class="form-control w-100" name="comment" required placeholder="Write here..."></textarea>
                        </div>
                        </form>
                    </div>

                    @endif
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
@endsection
