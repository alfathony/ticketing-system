
@extends('layouts/admin')

@section('title', 'Topic Detail')
@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">


    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
@endsection
@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height d-flex justify-content-center">
            <div class="col-md-6 col-12">
                @if(session('status'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{session('status')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if ($edit)
                        <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('ticket.update', $data->id)}}" method="post">
                            @method('PUT')
                @else
                        <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('ticket.store')}}" method="post">
                @endif
                    @csrf
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $edit ? 'Edit ticket' : 'Tambah ticket' }}</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <span>Topic</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <select name="topic_id" class="form-control" id="topic_id">
                                                        @foreach($topics as $value)
                                                            <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->topic_id) ? 'selected' : '' }}>{{ $value->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <span>Subject</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="text" id="title" name="title" class="form-control" value="{{ $edit ? $data->title : old('title') }}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <span>Message</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <div id="snow-container">
                                                        <div class="editor">
                                                            {!! $edit ?? '' ? $data->description : old('description')  !!}
                                                        </div>
                                                        <textarea name="description" id="description" class="d-none">
                                                            {{ $edit ?? '' ? $data->description : old('description') }}
                                                        </textarea>
                                                    </div>
                                                    <div class="caption text-danger mt-1">{{ $errors->first('description') }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <span>Attachment</span>
                                                </div>
                                                <div class="col-md-12">
                                                    @if (($data ?? '') && (null != $data->attachment))
                                                        <img src="{{ asset('storage/' . $data->attachment) }}" alt="" class="img-fluid rounded mb-3">
                                                    @endif
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="inputGroupFile01" name="attachment" value="{{ old('attachment') }}">
                                                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @if($edit)
                                        @cannot('isCustomer')
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <span>Status</span>
                                                </div>
                                                <div class="col-md-12">
                                                    <select name="status" class="form-control" id="status">
                                                        <option value="0" {{ ($edit && 0 == $data->status) ? 'selected' : '' }}>Open</option>
                                                        <option value="1" {{ ($edit && 1 == $data->status) ? 'selected' : '' }}>On Progress</option>
                                                        <option value="2" {{ ($edit && 2 == $data->status) ? 'selected' : '' }}>Closed</option>
                                                        <option value="3" {{ ($edit && 3 == $data->status) ? 'selected' : '' }}>Cancel</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <span>Tugaskan Teknisi</span>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <select name="teknisi_id" class="form-control" id="teknisi_id">
                                                            <option value="">-- Piih Teknisi --</option>
                                                            @foreach($teknisi as $value)
                                                                <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->teknisi_id) ? 'selected' : '' }}>{{ $value->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <span>Deadline</span>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="date" id="deadline" name="deadline" class="form-control pickadate-future" value="{{ $edit ? $data->deadline : old('deadline') }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endcannot
                                    @endif
                                </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        <span class="float-left">
                            <button type="reset" class="btn btn-outline-warning">Reset</button>
                        </span>
                        <span class="float-right">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </span>
                    </div>
                </div>
                </form>
            </div>
    </section>

@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/editors/quill/katex.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/highlight.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/quill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>



    <script>
        var editor = new Quill('#snow-container .editor', {
            bounds: '#snow-container .editor',
            modules: {

                toolbar: [
                    ['bold', 'italic', 'underline'],
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        var justHtmlContent = document.getElementById('description');

        editor.on('text-change', function() {
            var justHtml = editor.root.innerHTML;
            justHtmlContent.innerHTML = justHtml;
        });
    </script>
@endsection
