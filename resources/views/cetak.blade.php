<!DOCTYPE html>
<html>
<head>
    <title>Cetak Ticket {{$data->no_ticket}}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body onload="window.print()">
<style type="text/css">
    table tr td,
    table tr th{
        font-size: 9pt;
    }
</style>
<center>
    <h2>Ticket Laporan</h2>
    <p>{{$data->no_ticket}}</p>
</center>

<table class='table table-bordered'>
    <thead>
    <tr>
        <th>Nama</th>
        <th>Email</th>
        <th>Tanggal Masuk</th>
        <th>Customer Service</th>
        <th>Teknisi</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>

        <tr>
            <td>{{$data->user->name}}</td>
            <td>{{$data->user->email}}</td>
            <td>{{$data->created_at}}</td>
            <td>{{$data->admin->name}}</td>
            <td>{{$data->teknisi->name}}</td>
            <td>{!! $data->statusname !!}</td>
        </tr>

    </tbody>
</table>

<table class='table table-bordered'>
    <tr>
        <th>Deskripsi</th>
        <td>
            @if (($data ?? '') && (null != $data->attachment))
                <img src="{{ asset('storage/' . $data->attachment) }}" alt="" class="img-fluid rounded mt-2">
            @endif
            {!! $data->description !!}
        </td>
    </tr>
</table>

<h4>Comments</h4>

<table class='table table-bordered'>
    <thead>
    <tr>
        <th>Nama</th>
        <th>Tanggal</th>
        <th>Komentar</th>
    </tr>
    </thead>
    <tbody>

    @foreach($data->comments as $comment)
    <tr>
        <td>{{ $comment->user->name }} <span class="badge badge-light">{{ $comment->user->role->name }}</span></td>
        <td>{{ $comment->created_at }}</td>
        <td>{{ $comment->comment }}</td>
    </tr>
    @endforeach

    </tbody>
</table>

<h4>Laporan Teknisi</h4>

<table class='table table-bordered'>
    <thead>
    <tr>
        <th>Nama Teknisi</th>
        <th>Tanggal</th>
        <th>Komentar</th>
        <th>Photo</th>
    </tr>
    </thead>
    <tbody>

    @foreach($data->reports as $comment)
        <tr>
            <td>{{ $comment->teknisi->name }}</span></td>
            <td>{{ $comment->created_at }}</td>
            <td>{{ $comment->description }}</td>
            <td>
                @if (($comment ?? '') && (null != $comment->attachment))
                    <img src="{{ asset('storage/' . $comment->attachment) }}" alt="" class="img-fluid rounded mt-2">
                @endif
            </td>
        </tr>
    @endforeach

    </tbody>
</table>

</body>
</html>
