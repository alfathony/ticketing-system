@extends('layouts.admin')

@section('title', 'Dashboard')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header d-flex align-items-start pb-0">
                        <div>
                            <h2 class="text-bold-700 mb-0">{{ $totalCustomer }}</h2>
                            <p>Total Customer</p>
                        </div>
                        <div class="avatar bg-rgba-primary p-50 m-0">
                            <div class="avatar-content">
                                <i class="feather icon-users text-primary font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header d-flex align-items-start pb-0">
                        <div>
                            <h2 class="text-bold-700 mb-0">{{ $totalTeknisi }}</h2>
                            <p>Total Teknisi</p>
                        </div>
                        <div class="avatar bg-rgba-success p-50 m-0">
                            <div class="avatar-content">
                                <i class="feather icon-briefcase text-success font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header d-flex align-items-start pb-0">
                        <div>
                            <h2 class="text-bold-700 mb-0">{{ $totalTicket }}</h2>
                            <p>Total Tiket</p>
                        </div>
                        <div class="avatar bg-rgba-danger p-50 m-0">
                            <div class="avatar-content">
                                <i class="feather icon-box text-danger font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header d-flex align-items-start pb-0">
                        <div>
                            <h2 class="text-bold-700 mb-0">{{ $totalLaporan }}</h2>
                            <p>Total Laporan</p>
                        </div>
                        <div class="avatar bg-rgba-warning p-50 m-0">
                            <div class="avatar-content">
                                <i class="feather icon-bar-chart text-warning font-medium-5"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row match-height">
            <div class="col-md-8 col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between pb-0">
                        <h4 class="card-title">Support Tracker</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body pt-0">
                            <div class="row">
                                <div class="col-sm-2 col-12 d-flex flex-column flex-wrap text-center">
                                    <h1 class="font-large-2 text-bold-700 mt-2 mb-0">{{ $totalTicket }}</h1>
                                    <small>Tickets</small>
                                </div>
                                <div class="col-sm-10 col-12 d-flex justify-content-center">
                                    <div id="support-tracker-chart"></div>
                                </div>
                            </div>
                            <div class="chart-info d-flex justify-content-between">
                                <div class="text-center">
                                    <p class="mb-50">Open Tickets</p>
                                    <span class="font-large-1">{{ $openTicket }}</span>
                                </div>
                                <div class="text-center">
                                    <p class="mb-50">On Progress Tickets</p>
                                    <span class="font-large-1">{{ $onProgressTicket }}</span>
                                </div>
                                <div class="text-center">
                                    <p class="mb-50">Close Ticket</p>
                                    <span class="font-large-1">{{ $closeTicket }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-lg-4 col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-start">
                        <div>
                            <h4 class="card-title">Hot Topic</h4>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body px-0">
                            <div id="customer-chart"></div>
                        </div>
                        <ul class="list-group list-group-flush customer-info">
                            @foreach($hotTopic as $value)
                            <li class="list-group-item d-flex justify-content-between">
                                <div class="series-info">
                                    <i class="fa fa-circle font-small-3 text-primary"></i>
                                    <span class="text-bold-600">{{ $value->name }}</span>
                                </div>
                                <div class="product-result">
                                    <span>{{ $value->count }}</span>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


    @section('vendor-script')
        <!-- vendor files -->
            <script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
            <script src="{{ asset(mix('vendors/js/extensions/tether.min.js')) }}"></script>

        <!--<script src="{{ asset(mix('vendors/js/extensions/shepherd.min.js')) }}"></script>-->
    @endsection
    @section('page-script')
        <!-- Page js files -->
            <script>
                $(window).on("load", function () {

                    var $primary = '#7367F0';
                    var $danger = '#EA5455';
                    var $warning = '#FF9F43';
                    var $info = '#0DCCE1';
                    var $primary_light = '#8F80F9';
                    var $warning_light = '#FFC085';
                    var $danger_light = '#f29292';
                    var $info_light = '#1edec5';
                    var $strok_color = '#b9c3cd';
                    var $label_color = '#e7eef7';
                    var $white = '#fff';


                    // Subscribers Gained Chart starts //
                    // ----------------------------------

                    var gainedChartoptions = {
                        chart: {
                            height: 100,
                            type: 'area',
                            toolbar: {
                                show: false,
                            },
                            sparkline: {
                                enabled: true
                            },
                            grid: {
                                show: false,
                                padding: {
                                    left: 0,
                                    right: 0
                                }
                            },
                        },
                        colors: [$primary],
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'smooth',
                            width: 2.5
                        },
                        fill: {
                            type: 'gradient',
                            gradient: {
                                shadeIntensity: 0.9,
                                opacityFrom: 0.7,
                                opacityTo: 0.5,
                                stops: [0, 80, 100]
                            }
                        },
                        series: [{
                            name: 'New User',
                            data: $('#subscribe-gain-chart').data('chart')
                        }],

                        xaxis: {

                            labels: {
                                show: false,
                            },
                            axisBorder: {
                                show: false,
                            }
                        },
                        yaxis: [{
                            y: 0,
                            offsetX: 0,
                            offsetY: 0,
                            padding: { left: 0, right: 0 },
                        }],
                        tooltip: {
                            x: { show: false }
                        },

                    }

                    var gainedChart = new ApexCharts(
                        document.querySelector("#subscribe-gain-chart"),
                        gainedChartoptions
                    );

                    gainedChart.render();

                    // Subscribers Gained Chart ends //



                    // Orders Received Chart starts //
                    // ----------------------------------

                    var orderChartoptions = {
                        chart: {
                            height: 100,
                            type: 'area',
                            toolbar: {
                                show: false,
                            },
                            sparkline: {
                                enabled: true
                            },
                            grid: {
                                show: false,
                                padding: {
                                    left: 0,
                                    right: 0
                                }
                            },
                        },
                        colors: [$warning],
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            curve: 'smooth',
                            width: 2.5
                        },
                        fill: {
                            type: 'gradient',
                            gradient: {
                                shadeIntensity: 0.9,
                                opacityFrom: 0.7,
                                opacityTo: 0.5,
                                stops: [0, 80, 100]
                            }
                        },
                        series: [{
                            name: 'Transaction',
                            data: $('#orders-received-chart').data('chart')
                        }],

                        xaxis: {
                            labels: {
                                show: false,
                            },
                            axisBorder: {
                                show: false,
                            }
                        },
                        yaxis: [{
                            y: 0,
                            offsetX: 0,
                            offsetY: 0,
                            padding: { left: 0, right: 0 },
                        }],
                        tooltip: {
                            x: { show: false }
                        },
                    }

                    var orderChart = new ApexCharts(
                        document.querySelector("#orders-received-chart"),
                        orderChartoptions
                    );

                    orderChart.render();

                    // Orders Received Chart ends //



                    // Avg Session Chart Starts
                    // ----------------------------------

                    var sessionChartoptions = {
                        chart: {
                            type: 'bar',
                            height: 200,
                            sparkline: { enabled: true },
                            toolbar: { show: false },
                        },
                        states: {
                            hover: {
                                filter: 'none'
                            }
                        },
                        colors: [$label_color, $label_color, $primary, $label_color, $label_color, $label_color],
                        series: [{
                            name: 'Sessions',
                            data: [75, 125, 225, 175, 125, 75, 25]
                        }],
                        grid: {
                            show: false,
                            padding: {
                                left: 0,
                                right: 0
                            }
                        },

                        plotOptions: {
                            bar: {
                                columnWidth: '45%',
                                distributed: true,
                                endingShape: 'rounded'
                            }
                        },
                        tooltip: {
                            x: { show: false }
                        },
                        xaxis: {
                            type: 'numeric',
                        }
                    }

                    var sessionChart = new ApexCharts(
                        document.querySelector("#avg-session-chart"),
                        sessionChartoptions
                    );

                    sessionChart.render();

                    // Avg Session Chart ends //


                    // Support Tracker Chart starts
                    // -----------------------------

                    var supportChartoptions = {
                        chart: {
                            height: 270,
                            type: 'radialBar',
                        },
                        plotOptions: {
                            radialBar: {
                                size: 150,
                                startAngle: -150,
                                endAngle: 150,
                                offsetY: 20,
                                hollow: {
                                    size: '65%',
                                },
                                track: {
                                    background: $white,
                                    strokeWidth: '100%',

                                },
                                dataLabels: {
                                    value: {
                                        offsetY: 30,
                                        color: '#99a2ac',
                                        fontSize: '2rem'
                                    }
                                }
                            },
                        },
                        colors: [$danger],
                        fill: {
                            type: 'gradient',
                            gradient: {
                                // enabled: true,
                                shade: 'dark',
                                type: 'horizontal',
                                shadeIntensity: 0.5,
                                gradientToColors: [$primary],
                                inverseColors: true,
                                opacityFrom: 1,
                                opacityTo: 1,
                                stops: [0, 100]
                            },
                        },
                        stroke: {
                            dashArray: 8
                        },
                        series: [{{ ( ($totalTicket - $closeTicket) * 100) / $totalTicket }}],
                        labels: ['Completed Tickets'],

                    }

                    var supportChart = new ApexCharts(
                        document.querySelector("#support-tracker-chart"),
                        supportChartoptions
                    );

                    supportChart.render();

                    // Support Tracker Chart ends


                    // Product Order Chart starts
                    // -----------------------------

                    var productChartoptions = {
                        chart: {
                            height: 325,
                            type: 'radialBar',
                        },
                        colors: [$primary, $warning, $danger],
                        fill: {
                            type: 'gradient',
                            gradient: {
                                // enabled: true,
                                shade: 'dark',
                                type: 'vertical',
                                shadeIntensity: 0.5,
                                gradientToColors: [$primary_light, $warning_light, $danger_light],
                                inverseColors: false,
                                opacityFrom: 1,
                                opacityTo: 1,
                                stops: [0, 30]
                            },
                        },
                        stroke: {
                            lineCap: 'round'
                        },
                        plotOptions: {
                            radialBar: {
                                size: 165,
                                hollow: {
                                    size: '20%'
                                },
                                track: {
                                    strokeWidth: '100%',
                                    margin: 15,
                                },
                                dataLabels: {
                                    name: {
                                        fontSize: '18px',
                                    },
                                    value: {
                                        fontSize: '16px',
                                    },
                                    total: {
                                        show: true,
                                        label: 'Total',

                                        formatter: function (w) {
                                            // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                                            return 20
                                        }
                                    }
                                }
                            }
                        },
                        series: [5, 5, 5],
                        labels: ['Finished', 'Upcoming', 'Rejected'],

                    }

                    var productChart = new ApexCharts(
                        document.querySelector("#product-order-chart"),
                        productChartoptions
                    );

                    productChart.render();

                    // Product Order Chart ends //


                    // Sales Chart starts
                    // -----------------------------

                    var salesChartoptions = {
                        chart: {
                            height: 400,
                            type: 'radar',
                            dropShadow: {
                                enabled: true,
                                blur: 8,
                                left: 1,
                                top: 1,
                                opacity: 0.2
                            },
                            toolbar: {
                                show: false
                            },
                        },
                        toolbar: { show: false },
                        series: [{
                            name: 'Online',
                            data: [90, 50, 86, 40, 100, 20],
                        }, {
                            name: 'Offline',
                            data: [70, 75, 70, 76, 20, 85],
                        }],
                        stroke: {
                            width: 0
                        },
                        colors: [$primary, $info],
                        plotOptions: {
                            radar: {
                                polygons: {
                                    strokeColors: ['#e8e8e8', 'transparent', 'transparent', 'transparent', 'transparent', 'transparent'],
                                    connectorColors: 'transparent'
                                }
                            }
                        },
                        fill: {
                            type: 'gradient',
                            gradient: {
                                shade: 'dark',
                                gradientToColors: ['#9f8ed7', $info_light],
                                shadeIntensity: 1,
                                type: 'horizontal',
                                opacityFrom: 1,
                                opacityTo: 1,
                                stops: [0, 100, 100, 100]
                            },
                        },
                        markers: {
                            size: 0,
                        },
                        legend: {
                            show: true,
                            position: 'top',
                            horizontalAlign: 'left',
                            fontSize: '16px',
                            markers: {
                                width: 10,
                                height: 10,
                            }
                        },
                        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
                        dataLabels: {
                            style: {
                                colors: [$strok_color, $strok_color, $strok_color, $strok_color, $strok_color, $strok_color]
                            }
                        },
                        yaxis: {
                            show: false,
                        },
                        grid: {
                            show: false,
                        },

                    }

                    var salesChart = new ApexCharts(
                        document.querySelector("#sales-chart"),
                        salesChartoptions
                    );

                    salesChart.render();

                    // Customer Chart
                    // -----------------------------

                    var customerChartoptions = {
                        chart: {
                            type: 'pie',
                            height: 325,
                            dropShadow: {
                                enabled: false,
                                blur: 5,
                                left: 1,
                                top: 1,
                                opacity: 0.2
                            },
                            toolbar: {
                                show: false
                            }
                        },
                        labels: [
                            @foreach($hotTopic as $v)
                                '{{ $v->name }}',
                            @endforeach
                        ],
                        series: [
                            @foreach($hotTopic as $v)
                                {{ $v->count }},
                            @endforeach
                        ],
                        dataLabels: {
                            enabled: false
                        },
                        legend: { show: false },
                        stroke: {
                            width: 5
                        },
                        colors: [$primary, $warning, $danger],
                        fill: {
                            type: 'gradient',
                            gradient: {
                                gradientToColors: [$primary_light, $warning_light, $danger_light]
                            }
                        }
                    }

                    var customerChart = new ApexCharts(
                        document.querySelector("#customer-chart"),
                        customerChartoptions
                    );

                    customerChart.render();


                });
            </script>
@endsection

