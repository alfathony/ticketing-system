@php
$configData = Helper::applClasses();
@endphp
<div
  class="main-menu menu-fixed {{($configData['theme'] === 'light') ? "menu-light" : "menu-dark"}} menu-accordion menu-shadow"
  data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto">
        <a class="navbar-brand" href="{{ route('dashboard') }}">
          <div class="brand-logo"></div>
          <h2 class="brand-text mb-0">Ticket Aja</h2>
        </a>
      </li>
      <li class="nav-item nav-toggle">
        <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
          <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
          <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block white collapse-toggle-icon"
            data-ticon="icon-disc">
          </i>
        </a>
      </li>
    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

        <li class="nav-item not-active">
            <a href="{{ route('dashboard') }}">
                <i class="feather icon-home"></i>
                <span class="menu-title" data-i18n="">Dashboard</span>
            </a>
        </li>

        @cannot('isTeknisi')
        <li class="nav-item not-active">
            <a href="{{ route('ticket.index') }}">
                <i class="feather icon-package"></i>
                <span class="menu-title" data-i18n="">Ticket</span>
            </a>
        </li>
        @endcannot

        @cannot('isCustomer')
        <li class="nav-item not-active">
            <a href="{{ route('report.index') }}">
                <i class="feather icon-bar-chart"></i>
                <span class="menu-title" data-i18n="">Laporan</span>
            </a>
        </li>
        @endcannot

        @canany(['isTeknisi', 'isCustomer'])
        @else
            <li class="navigation-header">
                <span>Master Data</span>
            </li>

            <li class="nav-item not-active">
                <a href="{{ route('topic.index') }}">
                    <i class="feather icon-layers"></i>
                    <span class="menu-title" data-i18n="">Topics</span>
                </a>
            </li>

            <li class="nav-item not-active">
                <a href="{{ route('users.index') }}">
                    <i class="feather icon-users"></i>
                    <span class="menu-title" data-i18n="">Users</span>
                </a>
            </li>

            <li class="nav-item not-active">
                <a href="{{ route('roles.index') }}">
                    <i class="feather icon-shield"></i>
                    <span class="menu-title" data-i18n="">Roles</span>
                    {{--                    <span class="badge badge-pill badge-primary float-right notTest">9+</span>--}}
                </a>
            </li>
        @endcanany

{{--        <li class="nav-item not-active">--}}
{{--            <a href="#">--}}
{{--                <i class="feather icon-external-link"></i>--}}
{{--                <span class="menu-title" data-i18n="">Dropdown</span>--}}
{{--            </a>--}}

{{--            <ul class="menu-content">--}}
{{--                <li class="not-active">--}}
{{--                    <a href="#">--}}
{{--                        <i class="feather icon-user"></i>--}}
{{--                        <span class="menu-title" data-i18n="">Submenu</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </li>--}}

    </ul>
  </div>
</div>
<!-- END: Main Menu-->
