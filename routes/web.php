<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::resource('users', 'UsersController');
    Route::resource('roles', 'RolesController');
    Route::resource('topic', 'TopicController');
    Route::resource('ticket', 'TicketController');
    Route::resource('ticket-comments', 'TicketCommentsController');
    Route::resource('report', 'ReportController');

    Route::get('ticket-cetak/{id}', 'TicketController@cetak')->name('cetak.ticket');
});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');

Route::get('/foo', function () {
    Artisan::call('storage:link');
});
