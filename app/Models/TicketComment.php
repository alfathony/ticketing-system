<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TicketComment extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function ticket(){
        return $this->belongsTo(Ticket::class);
    }
}
