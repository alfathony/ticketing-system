<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function teknisi(){
        return $this->belongsTo(User::class, 'teknisi_id', 'id');
    }
}
