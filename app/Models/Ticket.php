<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function teknisi(){
        return $this->belongsTo(User::class, 'teknisi_id', 'id');
    }

    public function admin(){
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    public function topic(){
        return $this->belongsTo(Topic::class);
    }

    public function comments(){
        return $this->hasMany(TicketComment::class);
    }

    public function reports(){
        return $this->hasMany(Report::class);
    }

    //mutator

    public function getStatusNameAttribute()
    {
        if ($this->status == 0){
            return "<span class='badge badge-pill badge-primary'>Open</span>";
        }elseif ($this->status == 1){
            return "<span class='badge badge-pill badge-warning'>On Progress</span>";
        }elseif ($this->status == 2){
            return "<span class='badge badge-pill badge-success'>Closed</span>";
        }elseif ($this->status == 3){
            return "<span class='badge badge-pill badge-danger'>Cancel</span>";
        }else{
            return "<span class='badge badge-pill badge-light'>Undefined</span>";
        }
    }

    public function getNoTicketAttribute()
    {
        $prefix = "TA";
        $dateString = Carbon::parse($this->created_at)->format('Ymd');
        $id = $this->id;
        return "#" . $prefix . $dateString . "-" .$id;
    }
}
