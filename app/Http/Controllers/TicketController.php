<?php

namespace App\Http\Controllers;

use App\Models\MasterRoles;
use App\Models\Ticket;
use App\Models\Topic;
use App\User;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // access for admin and manager
        if (Auth::user()->role_id == 2 or Auth::user()->role_id == 4){
            if (isset($_GET['topic']) && $_GET['topic']){
                $datas = Ticket::where('topic_id', $_GET['topic'])->orderBy('created_at', 'DESC')->get();
            }else{
                $datas = Ticket::orderBy('created_at', 'DESC')->get();
            }
        }else{
            if (isset($_GET['topic']) && $_GET['topic']){
                $datas = Ticket::where('user_id', Auth::user()->id)->where('topic_id', $_GET['topic'])->orderBy('created_at', 'DESC')->get();
            }else{
                $datas = Ticket::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
            }
        }

        $topics = Topic::all();

        return view('admin.ticket.ticket', compact('datas', 'topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $topics = Topic::all();
        return view('admin.ticket.form', compact('edit', 'topics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Ticket();
        $data->topic_id = $request->topic_id;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->user_id = Auth::user()->id;

        if ($request->file('attachment')) {
            $file = $request->file('attachment')->store('attachment', 'public');
            $data->attachment = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('ticket.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Ticket::find($id);
        return view('admin.ticket.detail', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = Ticket::find($id);
        $topics = Topic::all();
        $teknisi = User::where('role_id', 3)->get();
        return view('admin.ticket.form', compact('edit','data', 'topics', 'teknisi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Ticket::find($id);
        $data->topic_id = $request->topic_id;
        $data->title = $request->title;
        $data->description = $request->description;

        if (null != $request->status){
            $data->status = $request->status;
        }

        $data->teknisi_id = $request->teknisi_id;
        $data->deadline = $request->deadline;
        $data->admin_id = Auth::user()->id; // admin who handled this issue

        if ($request->file('attachment')) {
            $file = $request->file('attachment')->store('attachment', 'public');
            $data->attachment = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('ticket.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Ticket::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('ticket.index');
    }

    /* Cetak tiket */
    public function cetak($id)
    {
        $data = Ticket::find($id);

        return view('cetak', compact('data'));

        // $pdf = PDF::loadview('cetak', ['data' => $data]);
        // return $pdf->download('laporan-ticket.pdf');
    }
}
