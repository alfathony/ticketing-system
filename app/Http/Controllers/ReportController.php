<?php

namespace App\Http\Controllers;

use App\Models\MasterRoles;
use App\Models\Report;
use App\Models\Ticket;
use App\Models\Topic;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id == 2 or Auth::user()->role_id == 4){
            if (isset($_GET['topic']) && $_GET['topic']){
                $datas = Ticket::where('teknisi_id', '!=', '')->where('topic_id', $_GET['topic'])->orderBy('created_at', 'DESC')->get();
            }else{
                $datas = Ticket::where('teknisi_id', '!=', '')->orderBy('created_at', 'DESC')->get();
            }
        }else{
            if (isset($_GET['topic']) && $_GET['topic']){
                $datas = Ticket::where('teknisi_id', Auth::user()->id)->where('topic_id', $_GET['topic'])->orderBy('created_at', 'DESC')->get();
            }else{
                $datas = Ticket::where('teknisi_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
            }

        }

        $topics = Topic::all();

        return view('admin.report.report', compact('datas', 'topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $topics = Topic::all();
        return view('admin.report.form', compact('edit', 'topics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Report();
        $data->ticket_id = $request->ticket_id;
        $data->description= $request->comment;
        $data->teknisi_id = Auth::user()->id;

        if ($request->file('attachment')) {
            $file = $request->file('attachment')->store('attachment', 'public');
            $data->attachment = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Ticket::find($id);
        return view('admin.report.detail', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = Report::find($id);
        $topics = Topic::all();
        $teknisi = User::where('role_id', 3)->get();
        return view('admin.report.form', compact('edit','data', 'topics', 'teknisi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Report::find($id);
        $data->topic_id = $request->topic_id;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->user_id = Auth::user()->id;
        $data->status = $request->status;
        $data->teknisi_id = $request->teknisi_id;

        if ($request->file('attachment')) {
            $file = $request->file('attachment')->store('attachment', 'public');
            $data->attachment = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('report.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Report::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('report.index');
    }
}
