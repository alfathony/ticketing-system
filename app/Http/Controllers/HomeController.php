<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totalCustomer = User::where('role_id', 1)->count();
        $totalTeknisi = User::where('role_id', 3)->count();
        $totalTicket = Ticket::count();
        $totalLaporan = Report::count();

        $openTicket = Ticket::where('status', 0)->count();
        $onProgressTicket = Ticket::where('status', 1)->count();
        $closeTicket = Ticket::where('status', 2)->count();

        $hotTopic = DB::table('tickets')
            ->select('name', DB::raw('COUNT(*) AS count'))
            ->join('topics', 'tickets.topic_id', '=', 'topics.id')
            ->groupBy('topic_id')
            ->orderBy('count', 'DESC')
            ->limit(3)
            ->get();

        // $hotTopic2 = DB::select('select topics.name, SUM( tickets.topic_id ) AS count from tickets inner join topics on tickets.topic_id = topics.id group by tickets.topic_id limit 3');

        // dd($hotTopic);

        return view('home', compact('totalCustomer', 'totalTeknisi', 'totalTicket', 'totalLaporan', 'openTicket', 'closeTicket', 'onProgressTicket', 'hotTopic'));
    }
}
