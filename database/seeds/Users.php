<?php

use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            [
                'name' => 'Joko Customer',
                'role_id' => 1,
                'email' => 'customer@ticketing.com',
                'password' => bcrypt('asdfasdf'),
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Agung Admin',
                'role_id' => 2,
                'email' => 'admin@ticketing.com',
                'password' => bcrypt('asdfasdf'),
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Udin teknisi',
                'role_id' => 3,
                'email' => 'teknisi@ticketing.com',
                'password' => bcrypt('asdfasdf'),
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Agus Manager',
                'role_id' => 4,
                'email' => 'manager@ticketing.com',
                'password' => bcrypt('asdfasdf'),
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
