<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTeknisiToTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->integer('teknisi_id')->nullable()->after('attachment');
            $table->date('deadline')->nullable()->after('teknisi_id');
            $table->integer('admin_id')->nullable()->after('teknisi_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('teknisi_id');
            $table->dropColumn('deadline');
            $table->dropColumn('admin_id');
        });
    }
}
